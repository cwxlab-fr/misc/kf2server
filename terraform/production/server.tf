resource "proxmox_lxc" "kf2_server" {

  target_node  = "node301"
  hostname     = "srv01.kf2.prod.priv.cwxlab.fr"
  ostemplate   = "local:vztmpl/ubuntu-20.04-standard_20.04-1_amd64.tar.gz"
  cores        = 4
  memory       = 6192
  swap         = 1144
  password     = var.admin_password
  unprivileged = true
  start        = true
  description  = "Serveur Killing Floor 2"

  ssh_public_keys = var.ssh_public_keys

  rootfs {
    storage = "local"
    size    = "8G"
 }

  mountpoint {
    key     = "0"
    slot    = 0
    storage = "local"
    mp      = "/opt/kf2server"
    size    = "50G"
  }

  network {
    name     = "eth0"
    bridge   = "vmbr202"
    ip       = "10.10.202.20/24"
    gw       = "10.10.202.240"
    firewall = false
  }
}
